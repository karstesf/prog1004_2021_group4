/**
* Software development project
*
*
* @file Project.cpp
* @author Nikolay Flaa Andersen
* @author Thea Urne
* @author Sindre Logstein
* @author Karsten Friis
*/


#include <iostream>              //  cout, cin
#include <fstream>               //  ifstream, ofstream
#include <string>                //  string
#include <vector>                //  vector
using namespace std;


class Task {
  private:
      string title,
             description,
             category;
      int    priority,
             status,
             deadline,
             startDate,
             finishDate;

  public:
      Task() { lesData();}
      Task(ifstream & inn);
      void lesData();
      void skrivData();
      void editData();
      int getPrio() { return priority;}
      string getCategory() {return category;}
      void skrivTilFil(ofstream & ut) const;
      void writeTitle();
      void deleteTask(const int task);
      void deleteGroupTask(const int task);

 };



void writeMenu();
void addTask();
void allTasks();
void editTask();
void lesFraFil(string username);
void skrivTilFil(string username);
void deleteTask();
int writePriority(char ting, int nr, int tall);
void writeDates(int date);

void groupMode (string username);
void groupAddTask(bool setting, string username, string groupName);
void groupAllTasks();
void groupEditTasks(bool setting, string username, string groupName);
void writeGroupMenu();
bool lesSetting(string groupName);
void groupSettings(bool& setting, string username, string groupName);
bool checkAdmin(string username, string groupName);
void groupLesFraFil(string groupName, string username);
void groupSkrivTilFil(string groupName, bool setting);
void groupDelete();
void groupDeleteTask(string username, string groupName);
void adminGroup(string username, string groupName);

char lesChar(const char* t);
int lesInt(const char* t, const int min, const int max);



vector <Task*> gTasks;                   ///< Vector with pointers to the task class

vector <Task*> gGroupTasks;  ///< Vector with pointers to the task class for groups


int main(){
    char choice;
    string username;

    cout << "Username (Empty for guest user): ";
    getline(cin, username);

    lesFraFil(username);                        // Reads from file for user
    writeMenu();
    choice = lesChar("Choice");

    while (choice != 'Q'){

        switch(choice){
            case 'N': addTask();            break;
            case 'A': allTasks();           break;
            case 'E': editTask();           break;
            case 'D': deleteTask();         break;
            case 'G': groupMode(username);  break;
            default: cout << "\tInvalid command";
                     writeMenu();           break;

        } choice = lesChar("\nChoice");

    }

    skrivTilFil(username);
    return 0;
}


// ---------------------------------------------------------------------------
//                       DEFINITION OF CLASS-FUNCTIONS:
// ---------------------------------------------------------------------------

Task::Task(ifstream & inn) {

    getline(inn, title);
    getline(inn, description);
    inn >> priority;    inn.ignore();
    inn >> status;      inn.ignore();
    getline(inn, category);
    inn >> deadline;    inn.ignore();
    inn >> startDate;   inn.ignore();
    inn >> finishDate;  inn.ignore();

}

                                    // Reads all the information
                                    // for a task from the user
void Task::lesData() {
    cout << "\t Title: "; getline(cin, title);
    cout << "\n\t Description: "; getline(cin, description);
    priority = lesInt("\n\t Priority", 1, 3);
    cout << "\n\t Category: "; getline(cin, category);
    deadline = lesInt("\n\t Deadline", 10121, 311299);
    startDate = lesInt("\n\t Start date", 10121, 311299);
    finishDate = lesInt("\n\t Finish date", 10121, 311299);
    status = 1;

}

                                                // Writes all the data about
                                                // a task to the screen
void Task::skrivData() {
    cout << "\n\nTitle: " << title << "\nCategory: " << category
    << "\nDescription: " << description << "\nPriority: ";
    if (priority == 1) cout << "Low";
    else if (priority == 2) cout << "Medium";
    else cout << "High";
    cout << "\nStatus: ";
    switch (status) {
        case 1 : cout << "Not Started"; break;
        case 2 : cout << "Started";     break;
        case 3 : cout << "On Hold";     break;
        case 4 : cout << "Finished";    break;
    }
    cout << "\nDeadline: "; writeDates(deadline);
    cout << "\nStart: "; writeDates(startDate);
    cout << "\nFinish: "; writeDates(finishDate);
}


void Task::editData() {
  char choice;

      cout << '\n';
      skrivData();
      cout << '\n';

        choice = lesChar("\nWhat do you want to edit?"
                        "\n\tT - Title \n\tD - Description \n\tP - Priority "
                        "\n\tC - Category \n\tS - Status "
                        "\n\tL - Deadline \n\tF - Finish date \nChoice");


      switch(choice){
        case 'T': {cout << "\nPrevious title:\t" << title;
                   cout << "\nNew title:\t"; getline(cin, title);} break;

        case 'D': {cout << "\nPrevious description:\t" << description;
                   cout << "\nNew description:\t"; getline(cin, description);} break;

        case 'P': {cout << "Previous priority:\t" << priority;
                   priority= lesInt("\nNew priority", 1, 3);} break;

        case 'C': {cout << "\nPrevious category:\t" << category;
                   cout << "\nNew category:\t"; getline(cin, category);} break;

        case 'S': {cout << "\n1 - Not Started \n2 - Started \n3 - On Hold \n4 - Finished\n";
                    cout << "\nPrevious status:\t" << status;
                   status = lesInt("\nNew status", 1, 4);} break;

        case 'L': {cout << "\nPrevious deadline:\t" << deadline;
                   deadline = lesInt("\nNew deadline", 10121, 311299);} break;

        case 'F': {cout << "\nPrevious finishdate:\t" << finishDate;
                   finishDate = lesInt("\nNewfinish date", 10121, 311299);} break;

        default: cout << "\nInvalid input\n"; break;
      }
 }


void Task::deleteTask(const int task){

    delete gTasks[task];
    gTasks.erase(gTasks.begin() + task);
    cout << "\n\tDeletion successful\n";
}


void Task::deleteGroupTask(const int task){

    delete gGroupTasks[task];
    gGroupTasks.erase(gGroupTasks.begin() + task);
    cout << "\n\tDeletion successful\n";

}


void Task::writeTitle(){
    cout << title;
}

                                                    // Writes info about a task
                                                    // to a file
void Task::skrivTilFil (ofstream & ut) const {

    ut  << '\n' << title << '\n' << description << '\n'
        << priority << ' ' << status << '\n' << category << '\n' << deadline << ' '
        << startDate << ' ' << finishDate;

    }



// ---------------------------------------------------------------------------
//                       DEFINITION OF OTHER FUNCTIONS:
// ---------------------------------------------------------------------------


void addTask() {

    cout << "New Task: \n";
    Task* newTask = new Task;
    gTasks.push_back(newTask);
    cout << "\n\tTask added successfully";

}

                                            // Writes all tasks to the screen
                                            // and sorts depending on user
                                            // input
void allTasks(){
 char answer;
 char ting = 'T';
 int nr = 1;
 string cat;

    if(!gTasks.empty()){

        do{
        answer = lesChar("Sort by: \n"
            "\tP - Priority\n"
            "\tC - Category\n"
            "\tN - No sorting\n"
            "Answer");
        } while ((answer != 'P') && (answer != 'C') && (answer != 'N'));


    switch (answer){
        case 'P':{
             cout << "\nAll Tasks Registered sorted by priority:\n\n";
            for (int i=3; i > 0; i--){
                nr = writePriority(ting, nr, i);
            }
                                                                break;}

        case 'C':{
                cout << "\nName of category you want to see: ";
                getline (cin, cat);

                for (int i = 0; i < gTasks.size(); i++){
                    if (cat == gTasks[i]->getCategory()){
                        cout << "\nNr: " << nr; nr++;
                        gTasks[i]->skrivData();
                        cout << "\n\n"; }
                } if (nr == 1){
                    cout << "\nCouldn't find any tasks with the written category\n";
                    }
                                                                 break;}

        case 'N': {
                cout << "\nAll Tasks Registered:\n\n";
                for (int i = 0; i < gTasks.size(); i++){
                    cout << "\nNr: " << i+1;
                    gTasks[i]->skrivData();
                    cout << "\n\n"; }
                                                                break;}

    }
    } else {
        cout << "\nNo tasks registered\n\n";
    }
}


void editTask() {

    int nr;

     if (!gTasks.empty()){
        for (int i=0; i < gTasks.size(); i++){
            cout << '\n' << i+1 << ". "; gTasks[i]->writeTitle();
        }
      nr = lesInt("\n\nWhich task do you want to edit?", 1, gTasks.size());
      gTasks[nr-1]->editData();
    } else cout << "There are no tasks to edit";
 }


void deleteTask(){
 int nr;
 char choice;

  if (!gTasks.empty()){
    for (int i=0; i < gTasks.size(); i++){
        cout << '\n' << i+1 << ". "; gTasks[i]->writeTitle();
      }

     nr = lesInt("\n\nWhich task do you want to delete?", 1, gTasks.size());
     gTasks[nr-1]->skrivData();
     choice = lesChar("\n\nAre you sure you want to delete the task? Y/N");
     if (choice == 'Y'){
       gTasks[nr-1]->deleteTask(nr-1);
     }else cout << "Not deleting this task";
  }else cout << "There are no tasks to delete";

}


void lesFraFil(string username){

                                                        // Reads from guest
    if (username.empty()){
        ifstream innfil("Guest.dta");

        if (innfil) {
            cout << "Reading data from Guest.dta\n";
            innfil.ignore();
            while (!innfil.eof()) {
                gTasks.push_back(new Task(innfil));
            }

            innfil.close();
        }
        else{                                       // Creating file if needed
            ofstream nyFil("Guest.dta");  nyFil.close();
            cout << "\n\Can't find the file 'Guest.dta'!\n\n"
                "This will now be created";
            }

    } else {
                                                    // Reads from user input
                                                    // username
        username += ".dta";
        ifstream innfil (username.c_str() );

        if (innfil){
            cout << "Reading data from " << username << "\n";
            innfil.ignore();
            while (!innfil.eof()) {
                gTasks.push_back(new Task(innfil));

            }
            innfil.close();
        } else {
            ofstream nyFil(username.c_str()); nyFil.close();
            cout << "\nCan't find the file '" << username << "'\n\n"
                    "This will now be created";
        }

    }
}


void skrivTilFil(string username){

    if (username.empty()){
        ofstream utfil("Guest.dta");

        for (int i = 0; i < gTasks.size(); i++){
            gTasks[i]->skrivTilFil(utfil);
            }

        cout << "Data written to Guest.dta\n";


        utfil.close();

    } else {
        username += ".dta";
        ofstream utfil (username.c_str());

        for (int i = 0; i < gTasks.size(); i++){
            gTasks[i]->skrivTilFil(utfil);
            }
        cout << "Data written to " << username;

        utfil.close();
    }
}


int writePriority(char ting, int nr, int tall){

                                                // Goes through the vector
                                                // and writes the task
                                                // depending on the priority
    switch (ting){
    case 'T':{
            for (int i = 0; i < gTasks.size(); i++){
                if (tall == gTasks[i]->getPrio()){
                    cout << "\nNr: " << nr; nr++;
                    gTasks[i]->skrivData();
                    cout << "\n\n"; }
                } return nr;
                                break;}
                                                // For group, above is for users

    case 'G':{
            for (int i = 0; i < gGroupTasks.size(); i++){
                if (tall == gGroupTasks[i]->getPrio()){
                    cout << "\nNr: " << nr; nr++;
                    gGroupTasks[i]->skrivData();
                    cout << "\n\n"; }
                } return nr;

                    break;}
    }
}


void writeDates(int date){                       // Writes the dates from a
                                                 // 6 length integer

    cout << date / 100000 % 10 << date / 10000 % 10 << "/"
         << date / 1000 % 10 << date / 100 % 10 << "/"
         << date / 10 % 10 << date % 10;
}


void writeMenu(){

    cout << "\nMenu:\n"
         << "\tN - New task\n"
         << "\tA - Write all tasks\n"
         << "\tE - Change a task\n"
         << "\tD - Delete a task\n"
         << "\tG - Enter group mode\n"
         << "\tQ - Quit\n\n";
}





void groupMode(string username){
 string groupName, groupAdmin;
 char choice;
 bool setting;

    cout << "Group name you want to enter/create: ";
    getline (cin, groupName);

                                                    // Reads the tasks from file
                                                    // and the setting, admin or
                                                    // everyone mode
    groupLesFraFil(groupName, username);
    setting = lesSetting(groupName);

    writeGroupMenu();
    choice = lesChar("Choice");

    while (choice != 'Q'){

        switch(choice){
            case 'N': groupAddTask(setting, username, groupName);   break;
            case 'A': groupAllTasks();                              break;
            case 'E': groupEditTasks(setting, username, groupName); break;
            case 'D': groupDeleteTask(username, groupName);         break;
            case 'C': groupSettings(setting, username, groupName);  break;
            case 'M': adminGroup(username, groupName);              break;
            default:  cout << "\tInvalid command";
                      writeGroupMenu();                             break;

        } choice = lesChar("\nChoice");

    }
                                            // Writes to file and
                                            // deletes it from the vector
                                            // or else there would be duplicates
    groupSkrivTilFil(groupName, setting);
    groupDelete();
    writeMenu();

}


void groupAddTask(bool setting, string username, string groupName) {
 bool ting;

    if (setting == 0){
        cout << "New Task: \n";
        Task* newTask = new Task;
        gGroupTasks.push_back(newTask);
        cout << "\n\tTask added successfully";
    }
    else if (setting == 1){                     // If the group is in admin mode
                                                // check if the user is an admin
        ting = checkAdmin(username, groupName);
        if (ting){
            cout << "New Task: \n";
            Task* newTask = new Task;
            gGroupTasks.push_back(newTask);
            cout << "\n\tTask added successfully";

        } else {
            cout << "The group is in admin mode and the user is not an admin\n\n";
        }
    }
}


void groupAllTasks(){
 char answer;
 char ting = 'G';
 int nr = 1;
 string cat;

    if(!gGroupTasks.empty()){
        do{
        answer = lesChar("Sort by: \n"
            "\tP - Priority\n"
            "\tC - Category\n"
            "\tN - No sorting\n"
            "Answer");
        } while ((answer != 'P') && (answer != 'C') && (answer != 'N'));


    switch (answer){
        case 'P':{
             cout << "\nAll Tasks Registered sorted by priority:\n\n";
            for (int i=3; i > 0; i--){
                nr = writePriority(ting, nr, i);
            }
                                                                break;}

        case 'C':{
                cout << "\nName of category you want to see: ";
                getline (cin, cat);

                for (int i = 0; i < gGroupTasks.size(); i++){
                    if (cat == gGroupTasks[i]->getCategory()){
                        cout << "\nNr: " << nr; nr++;
                        gGroupTasks[i]->skrivData();
                        cout << "\n\n"; }
                } if (nr == 1){
                    cout << "\nCouldn't find any tasks with the written category\n";
                    }
                                                                 break;}

        case 'N': {
                cout << "\nAll Tasks Registered:\n\n";
                for (int i = 0; i < gGroupTasks.size(); i++){
                    cout << "\nNr: " << i+1;
                    gGroupTasks[i]->skrivData();
                    cout << "\n\n"; }
                                                                break;}

        }
    } else {
        cout << "\nNo tasks registered\n\n";
    }
}


void groupEditTasks(bool setting, string username, string groupName) {
 int nr;
 bool ting;

    if (setting == 0){
        if (!gGroupTasks.empty()){
            nr = lesInt("\nWhich task do you want to edit?", 1, gGroupTasks.size());
            gGroupTasks[nr-1]->editData();
        } else cout << "There are no tasks to edit";
    }
    else if (setting == 1){
        ting = checkAdmin(username, groupName);
        if (ting){
            if (!gGroupTasks.empty()){
                nr = lesInt("\nWhich task do you want to edit?", 1, gGroupTasks.size());
                gGroupTasks[nr-1]->editData();
            } else cout << "There are no tasks to edit";

        } else {
            cout << "The group is in admin mode and the user is not an admin\n\n";
        }
    }
 }


void groupSettings(bool& setting, string username, string groupName){
 char answer;
 bool ting;

    if (setting == 1){
        cout << "\n\nThe group is currently in Admin mode\n\n";

    } else if (setting == 0){
        cout << "\n\nThe group is currently in Everyone mode\n\n";
    }

                                                    // Checks if the current user
                                                    // is an admin
    ting = checkAdmin(username, groupName);
    if (ting){
        do {
            cout << "Admin mode - only admins can add and edit tasks.\n"
            "Everyone mode - everyone in the group can add and edit tasks.\n\n";

            answer = lesChar("Type 'A' for Admin mode "
                             "or 'E' for Everyone mode:");
        } while ((answer != 'A') && (answer != 'E'));

        if (answer == 'A'){
            setting = true;

        } else if (answer == 'E'){
            setting = false;
        }
    } else {
        cout << "User has to be an admin to change settings\n";
    }

}



void groupLesFraFil(string groupName, string username) {
 string groupAdmin;

    groupAdmin = groupName + "admin.dta";
    groupName += ".dta";

    ifstream innfil (groupName.c_str());

    if (innfil){
            innfil.ignore(); innfil.ignore();
            while (!innfil.eof()) {
                gGroupTasks.push_back(new Task(innfil));
            }

            innfil.close();

                                        // If it can't find the file to read from
                                        // it will be created, together with an
                                        // admin file
    } else {
        ofstream nyFil(groupName.c_str()); nyFil << '1'; nyFil.close();
        cout << "Couldn't find the group file '" << groupName << "'\n\n"
                "This will now be created";
        ofstream adminFil (groupAdmin.c_str()); adminFil << username;
                                                adminFil.close();
    }
}


void groupSkrivTilFil(string groupName, bool setting){

        groupName += ".dta";
        ofstream utfil (groupName.c_str());

        if (setting == 1){
            utfil << '1';
        } else if (setting == 0){
            utfil << '0';
        }

        for (int i = 0; i < gGroupTasks.size(); i++){
            gGroupTasks[i]->skrivTilFil(utfil);
            }
        cout << "Data written to " << groupName << "\n";

        utfil.close();
}


                                                        // Reads the group
                                                        // setting from file
bool lesSetting(string groupName){
 int tall;

    groupName += ".dta";

    ifstream innfil (groupName.c_str());

    if (innfil){
           innfil >> tall;

           if (tall == 1){
            return true;
           } else if (tall == 0){
            return false;
           }

            innfil.close();
    }

}

                                                        // Checks if user is an
                                                        // admin in current group
bool checkAdmin(string username, string groupName){
 string groupAdmin, name;

    groupAdmin = groupName + "admin.dta";

    ifstream innfil (groupAdmin.c_str());

    if (innfil){
            while (!innfil.eof()) {
                innfil >> name;
                if (username == name){
                    return true;
                }
            }

            innfil.close();
            return false;
    }
}


void groupDelete(){

    if (gGroupTasks.size() > 0){
        for (int i = 0; i < gGroupTasks.size(); i++){
            delete gGroupTasks[i];
        }
        gGroupTasks.clear();

    }
}


void groupDeleteTask(string username, string groupName){
int nr;
char choice;
bool ting;

    ting = checkAdmin(username, groupName);

    if (ting){
    if (!gGroupTasks.empty()){
        for (int i=0; i < gGroupTasks.size(); i++){
            cout << '\n' << i+1 << ". "; gGroupTasks[i]->writeTitle();
        }
            nr = lesInt("\n\nWhich task do you want to delete?", 1, gGroupTasks.size());
            gGroupTasks[nr-1]->skrivData();
            choice = lesChar("\n\nAre you sure you want to delete the task? Y/N");
            if (choice == 'Y'){
                gGroupTasks[nr-1]->deleteGroupTask(nr-1);
            }else cout << "Not deleting this task";
        }else cout << "There are no tasks to delete";
    } else cout << "User has to be an admin to delete tasks";
}


void adminGroup(string username, string groupName){
 bool ting, sjekk;
 char answer;
 string groupAdmin, slettNavn, navn;

    groupAdmin = groupName + "admin.dta";
    ifstream innfil (groupAdmin.c_str());
    ofstream temp ("temp.txt");
    ofstream utfil (groupAdmin.c_str(), std::ios::app);

    ting = checkAdmin(username, groupName);
    if (ting){
        do {
            answer = lesChar("\nType 'A' to add an Admin "
                             "or 'R' to remove an Admin:");
        } while ((answer != 'A') && (answer != 'R'));

                                                // Adds new admin to the group
        if (answer == 'A'){
            cout << "\nName of new Admin: ";
            getline (cin, navn);

            utfil << '\n' << navn;
            cout << "\n" << navn << " added as an Admin";
            innfil.close();
            utfil.close();
            temp.close();
            remove ("temp.txt");

                                                // Removes admin from the group
        } else if (answer == 'R'){
            cout << "\nName of Admin to remove: ";
            getline (cin, slettNavn);
            sjekk = checkAdmin(slettNavn, groupName);
            if (sjekk){
                while (!innfil.eof()){
                    getline(innfil, navn);
                    if (!(navn == slettNavn)){
                        if (!navn.empty()){
                        temp << navn << endl;
                        }
                    } else {
                        cout << "\n" << slettNavn << " removed as admin";
                    }
                }

                innfil.close();
                utfil.close();
                temp.close();
                remove (groupAdmin.c_str());
                rename ("temp.txt",groupAdmin.c_str());
            } else {
                cout << "This user is not an admin";
            }

        }



    } else {
        cout << "\nUser is not an admin and cannot add/remove admins";
    }


}

void writeGroupMenu(){

    cout << "\n\nGroup menu:\n"
         << "\tN - New task\n"
         << "\tA - Write all tasks\n"
         << "\tE - Edit a task\n"
         << "\tD - Delete a task\n"
         << "\tC - Change group settings\n"
         << "\tM - Add or remove admins\n"
         << "\tQ - Quit\n\n";

}








char lesChar(const char* t)  {
     char tegn;
     cout << t << ":  ";
     cin >> tegn;  cin.ignore(256, '\n');
     return (toupper(tegn));
}


int lesInt(const char* t, const int min, const int max)  {
    char buffer[256];
    int  tall;
    bool feil;

    do {
        feil = false;
        cout << t << " (" << min << " - " << max << "):  ";
        cin.getline(buffer, 256);
        tall = atoi(buffer);
        if (tall == 0 && buffer[0] != '0')
        {  feil = true;   std::cout << "\nERROR: Not an integer\n\n";  }
    } while (feil  ||  tall < min  ||  tall > max);

    return tall;
}
